package com.deveducation.www.guessnumber;

import com.deveducation.www.guessnumber.memory.GenerateRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


class GenerateRandomTest {

    static Arguments[] getNextRandomArgs() {
        return new Arguments[]{
                Arguments.arguments(1, 200),
                Arguments.arguments(3, 55)
        };
    }

    @ParameterizedTest
    @MethodSource("getNextRandomArgs")
    void getNextRandom(int start, int end) {
        GenerateRandom cut = new GenerateRandom(start, end);
        int expected = cut.getNextRandom();

        Assertions.assertTrue(start < expected && expected < end);
    }
}

