package com.deveducation.www.guessnumber.service;

import com.deveducation.www.guessnumber.memory.GenerateRandom;

import java.util.Scanner;

import static com.deveducation.www.guessnumber.constant.Constant.*;

public class Logic {

    private GenerateRandom searchPool;
    private final Scanner scanner = new Scanner(System.in);
    private boolean endGame = false;
    private boolean correctAnswer = false;
    private int hiddenNumber;
    private int stepTimesToGuess;



    private boolean correctingMSG = false;

    public void menuCycle(){
        System.out.println(MSG_WELCOME);
        String choice;
            searchPool = null;
            choice = scanner.nextLine();
            switch (choice) {
                case CMD_START:
                    start();
                    break;
                case CMD_HELP:
                    help();
                    break;
                case CMD_RULES:
                    rules();
                    break;
                case CMD_EXIT:
                    System.exit(0);
                    return;
                default:
                    if(correctingMSG==true){
                        System.out.println(CMD_INVALID_CHOICE);
                    }
                    correctingMSG=true;
                    break;
            }
    }


    private void help(){
        System.out.println(CMD_LIST_OF_COMMANDS);
    }


    private void rules(){
        System.out.println(MSG_RULES_IN_THIS_GAME);
    }

    private void start(){

        settingSearchPool();
        if(searchPool!=null){
            gameCycle();
        }
    }

    private void gameCycle(){
        do {
            checkIfOutOfGuesses();
            String command = scanner.next();
            checkIfThereINT(command);
        }while (!correctAnswer);


    }
// задаем граничные условия для запуска дальнейшей программы и собственно начинаем игру
    private void settingSearchPool(){
        System.out.println(MSG_GAME_START);

        try{

        int startLine = scanner.nextInt();
        int endLine = scanner.nextInt();
        int numberOfTimesToTry = scanner.nextInt();

        if(startLine < 201 && endLine < 201 && startLine> 0 && endLine > 0 && startLine != endLine
        && 0 < numberOfTimesToTry && numberOfTimesToTry <= 15){
            searchPool = new GenerateRandom(startLine,endLine,numberOfTimesToTry);
        }else{
            System.out.println(MSG_ERROR);
            searchPool = null;
            correctingMSG=false;
            return;
        }}catch (Exception e){
            System.out.println(MSG_ERROR_SETTING);
            searchPool=null;
            correctingMSG=false;
            return;

        }

        System.out.println(MSG_START_GAME+searchPool.getStart()+" to "+searchPool.getEnd()+"." +
                MSG_TRY_GUESS+searchPool.getTryingToGuess()+" attempts!");
        stepTimesToGuess =searchPool.getTryingToGuess();
        hiddenNumber = searchPool.getNextRandom();
    }


    private void checkIfThereINT(String command){
                   try{
                int currentGivenNumber = Integer.parseInt(command);
                searchPool.setReceivedAnswer(currentGivenNumber);
                searchPool.setTryingToGuess(searchPool.getTryingToGuess()-1);
                compareWithPreviousAnswerAndCheckDigit(currentGivenNumber);
                searchPool.setPreviousHiddenNumber(searchPool.getReceivedAnswer());

            }

            catch (Exception e){
                if(command.equalsIgnoreCase(CMD_EXIT)){
                    System.out.println(MSG_GAME_END);
                    System.exit(0);
                }else{
                    System.out.println(command);
                    System.out.println(MSG_GAME_ORE_END);
                }
            }

    }

    private void compareWithPreviousAnswerAndCheckDigit(int givenAnswer){
        if(givenAnswer == hiddenNumber){
            System.out.println(MSG_VICTORY+ hiddenNumber +") in "+(stepTimesToGuess -searchPool.getTryingToGuess())+" tries!");
            correctAnswer = true;
            System.exit(0);
            return;
        }

        if(searchPool.getPreviousHiddenNumber()==0){
            System.out.println(MSG_FIRST_ANSWER+givenAnswer);
        }
// если есть уже ответ кроме первого то происходит сравнение и вывод Colder ore  Warmer
        if(searchPool.getPreviousHiddenNumber()!=0){
            if(Math.abs(hiddenNumber -givenAnswer) == Math.abs(hiddenNumber -searchPool.getPreviousHiddenNumber()))
            {
                System.out.println("Same distance. "+searchPool.getTryingToGuess()+" guesses left");
            }

            if(Math.abs(hiddenNumber -givenAnswer) > Math.abs(hiddenNumber -searchPool.getPreviousHiddenNumber()))
            {
                System.out.println("Colder… "+"\uD83D\uDE2C"+searchPool.getTryingToGuess()+" guesses left");
            }

            if(Math.abs(hiddenNumber -givenAnswer) < Math.abs(hiddenNumber -searchPool.getPreviousHiddenNumber()))
            {
                System.out.println("Warmer! "+"\u2764 "+searchPool.getTryingToGuess()+" guesses left");
            }
        }
    }

    private void checkIfOutOfGuesses(){

            if(searchPool.getTryingToGuess()==0){
                System.out.print(MSG_RUN_OUT_OF_GUESS);
                System.out.println(MSG_SECRET_NUMBER + hiddenNumber);
            }

    }









}
