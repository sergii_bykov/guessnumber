package com.deveducation.www.guessnumber.constant;

public class Constant {
    public static final String CMD_EXIT = "exit";
    public static final String CMD_START = "start";
    public static final String CMD_HELP = "help";
    public static final String CMD_RULES = "rules";
    public static final String CMD_INVALID_CHOICE = "Invalid choice. Use 'help' if needed.";
    public static final String CMD_LIST_OF_COMMANDS = "List of commands:\n start \n help \n rules  \n exit ";
    public static final String MSG_RULES_IN_THIS_GAME = "Enter 'start' to set the search pool\n " +
            "START line < END line. Range must be set in INT type \n " +
            "START must be equal or greater than 1 \n" +
            "END must be equal or lesser than 200  \n " +
            "Number of guesses can range from 1 to 15";
    public static final String MSG_WELCOME = "Enter: help - to see list of command";
    public static final String MSG_ERROR = "An error occurred while entering data. Please read the rules. Enter help";
    public static final String MSG_ERROR_SETTING = "There was error at setting search pool constructor. Please read rules. Enter help";
    public static final String MSG_START_GAME = "Hi, I have a secret number from ";
    public static final String MSG_TRY_GUESS = "Try to guess it with ";
    public static final String MSG_GAME_END ="Game over . Bye";
    public static final String MSG_GAME_ORE_END ="Enter number INT to guess or 'exit'-end game ";
    public static final String MSG_VICTORY ="Victory!!! You have guessed number (";
    public static final String MSG_FIRST_ANSWER ="Your first answer ";
    public static final String MSG_RUN_OUT_OF_GUESS = "You have run out of guesses. ";
    public static final String MSG_SECRET_NUMBER = "Secret number was ";
    public static final String MSG_GAME_START = "Please enter: \n -START INT \n -END INT \n -number of guesses - max.15";
}
