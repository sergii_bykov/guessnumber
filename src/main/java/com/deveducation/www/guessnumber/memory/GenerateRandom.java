package com.deveducation.www.guessnumber.memory;

public class GenerateRandom {

    private int start;
    private int end;
    private int tryingToGuess;
    private int receivedAnswer;
    private int previousHiddenNumber;

    public GenerateRandom(int start, int end, int tryingToGuess) {
        this.start = start;
        this.end = end;
        this.tryingToGuess = tryingToGuess;
    }

    public GenerateRandom(int start,int end){
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getTryingToGuess() {
        return tryingToGuess;
    }

    public void setTryingToGuess(int tryingToGuess) {
        this.tryingToGuess = tryingToGuess;
    }

    public int getReceivedAnswer() {
        return receivedAnswer;
    }

    public void setReceivedAnswer(int receivedAnswer) {
        this.receivedAnswer = receivedAnswer;
    }

    public int getPreviousHiddenNumber() {
        return previousHiddenNumber;
    }

    public void setPreviousHiddenNumber(int previousHiddenNumber) {
        this.previousHiddenNumber = previousHiddenNumber;
    }


    public int getNextRandom(){

        int random = Math.abs(start) + (int)(Math.random() * (Math.abs(end - start) + 1));


        return random;
    }
}
